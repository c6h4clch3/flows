# サインアップ手順

## 1. トップページ

[GitLab トップページ](https://gitlab.com)

![トップページ](./imgs/top-page.png)

画面右上の「Register」ボタンをクリック。

## 2. ソーシャルログイン

![ソーシャルログイン](./imgs/social-login.png)

画面下部の「Sign in with」に並んでるボタンから Twitter ボタンをクリック。

Twitter のログインと外部アプリの認可画面が要求されるので、許可する。
その後自動的に GitLab の画面に戻る。

## 3. 利用規約に同意

![利用規約](./imgs/accept-terms.png)

緑色の「Accept terms」をクリックして利用規約に同意。

## 4. Eメールアドレス設定

![Eメールアドレス](./imgs/email-setting.png)

E メールアドレスが必須なので設定して、画面下の方の緑色のボタン「Save changes」をクリック。

![UserID](./imgs/user-id.png)
右側の「User ID」をコピーしてミナトまで共有ください。Twitter でも Discord でもいいです。

## 5. 言語設定

言語設定が気になる場合は「Preferences」から設定可能です。

![言語設定](./imgs/language-setting.png)

1. ユーザー設定画面左側のメニューから「Preferences」をクリック
2. 下の方の「Localization」セクションの「Language」のプルダウンから日本語を選択
3. その下の緑色のボタン「Save changes」をクリックして設定を保存

ただし、言語設定も十分じゃないしそもそもプログラマのための機能がいっぱいなのであまりわからん単語が並ぶ事態は変わらないかも。
なのでわからないことがあれば逐次ミナトまで。
